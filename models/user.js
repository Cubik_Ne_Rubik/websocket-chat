var crypto = require('crypto');
var mongoose = require('../libs/mongoose');
var Schema = mongoose.Schema;
var async = require('async');

var model = new Schema({
    username:{
        type: String,
        unique: true,
        required: true
    },
    hashedPassword:{
        type: String,
        required: true
    },
    salt:{
        type: String,
        required: true
    }
});

model.methods.getPassword = function(password){
    return crypto.createHmac('sha1', this.salt).update(password).digest('hex');
};

model.virtual('password')
.set(function(password){
        this._plainPassword = password;
        this.salt = Math.random() + '';
        this.hashedPassword = this.getPassword(password);
    })
.get(function(){ return this._plainPassword;});

model.methods.checkPassword = function(password){
    return this.getPassword(password) === this.hashedPassword;
};

model.statics.authorize = function(username, password, callback){
    var User = this;
    async.waterfall([
        function(callback){
            User.findOne({username: username}, callback);
            console.log('user find');
        },
        function(user, callback){
            if(user){
                console.log('user true');
                if(user.checkPassword(password)){
                    callback(null, user);
                } else {
                    callback ( new Error('Password failed'))
                }
            } else {
                console.log('user false');
                var user = new User({username: username, password: password});
                user.save(function (err){
                    console.log('user save');
                    if (err) return callback(err);
                    console.log('user save1');
                    callback(null, user);
                });
            }
        }
    ], callback);
};
exports.User = mongoose.model('User', model);