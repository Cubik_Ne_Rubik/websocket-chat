var express = require('express');
var User = require('../models/user').User;
var router = express.Router();

/* GET chat. */
router.get('/chat', function(req, res, next) {
  if(req.user) //authorize
  {
    res.render('chat');
  }else{ //no-authorize
    res.redirect('/login');
  }
});

router.get('/login', function(req, res, next) {
  if(req.user) //authorize
  {
    res.redirect('/chat');
  }else{ //no-authorize
    res.render('login');
  }
});

router.post('/login', function(req, res, next) {
  var username = req.body.Email;
  var password = req.body.Password;
  if(password) {
    User.authorize(username, password, function(err, user){
      if(err) return next(err);
      req.session.user = user._id;
      res.redirect('/chat');
    })
  }else{
    res.render('login');
  }
});

router.post('/logout', function(req, res, next) {
  console.log('logout');
  req.session.destroy();
  res.redirect('/login');
});

//if route undefined
router.use(function(req, res, next) {
  if(req.user) //authorize
  {
    res.redirect('/chat');
  }else{ //no-authorize
    res.redirect('/login');
  }
});

module.exports = router;
