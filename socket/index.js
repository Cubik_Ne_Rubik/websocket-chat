var cookie = require('cookie');
var async = require('async');
var User = require('../models/user').User;
var config = require('../config');
var sessionStore = require('../libs/sessionStore');
var cookieParser = require('cookie-parser');

function loadSession(sid, callback){
    sessionStore.load(sid, function(err, session){
        if(arguments.length == 0){
            return callback(null, null);
        }else{
            return callback(null, session)
        }
    });
} // for correct callback

function loadUser(session, callback){
    if(!session.user){
        return callback(null,null);
    }

    User.findById(session.user, function(err, user){
        if(err) return callback(err);
        if(!user){
            return callback(null,null);
        }
        callback(null, user);
    })
}

module.exports = function (server){
    var io = require('socket.io').listen(server);
    var sockets = [];
    io.set('authorization', function(handshakeData,callback){
        async.waterfall([
            function(callback){
                handshakeData.cookies = cookie.parse(handshakeData.headers.cookie || '');
                var sidCookie = handshakeData.cookies[config.get('session:key')];
                var sid = cookieParser.signedCookie(sidCookie, config.get('session:secret'));
                loadSession(sid, callback);
            },
            function(session, callback){
                if(!session) {
                    return callback(new Error(401, "No session"));
                }

                handshakeData.session = session;
                loadUser(session, callback);
            },
            function(user, callback){
                if(!user) {
                    return callback(new Error(403, "Forbidden"));
                }
                handshakeData.user = user;
                callback(null, true);
            }
        ], function(err){
            if(!err){
                return callback(null, true);
            }
            if(err instanceof Error){
                return callback(null, false);
            }
            console.log(err);
            callback(err);
        })
    })

    io.sockets.on('connection', function (socket){
        var username = socket.request.user.get('username');
        sockets.push(socket);

        socket.broadcast.emit('join', username);

        socket.on('message', function(text, callback){
            socket.broadcast.emit('message', username, text);
            callback(text, username);
        });

        socket.on('privateMessage', function(msg, callback){
            var sended = false;
            for(var i=0; i < sockets.length; ++i){
                var tmp = sockets[i];
                if(tmp.request.user.get('username') === msg.receiver){
                    msg.sender = username;
                    tmp.emit('privateMessage', msg);
                    sended = true;
                    break;
                }
            }
            callback(!sended, username);
        });

        socket.on('getUserList', function(){
            var userList = [];
            for(var i=0; i < sockets.length; ++i){
                var tmp = sockets[i];
                userList.push(tmp.request.user.get('username'));
            }
            socket.emit('UserList', userList);
        });

        socket.on('disconnect', function(){
            sockets.splice(sockets.indexOf(socket), 1);
            socket.broadcast.emit('leave', username);
        });
    });
};

